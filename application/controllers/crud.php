<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {
	public function index(){
		//echo "<h2>Hallo CRUD<h2/>";
		$data = $this->mymodel->GetAnggota();	
		$this->load->view('tabel',array('data' => $data));
	}
	public function add_data(){
		echo "<h2>Tambah Data</h2>";
		$this->load->view('form_add');
	}
	public function do_insert(){
		$id_anggota = $_POST['id_anggota'];
		$nama_anggota = $_POST['nama_anggota'];
		$alamat_anggota = $_POST['alamat_anggota'];
		$tempat_lahir_anggota = $_POST['tempat_lahir_anggota'];
		$tgl_lahir_anggota = $_POST['tgl_lahir_anggota'];
		$jenis_kelamin_anggota = $_POST['jenis_kelamin_anggota'];
		$status_anggota = $_POST['status_anggota'];
		$no_telp_anggota = $_POST['no_telp_anggota'];
		$ket_anggota = $_POST['ket_anggota'];
		$rekening = $_POST['rekening'];
		$data_insert = array(
		'id_anggota' => $id_anggota,
		'nama_anggota' => $nama_anggota,
		'alamat_anggota' => $alamat_anggota,
		'tempat_lahir_anggota' => $tempat_lahir_anggota,
		'tgl_lahir_anggota' => $tgl_lahir_anggota,
		'jenis_kelamin_anggota' => $jenis_kelamin_anggota,
		'status_anggota' => $status_anggota,
		'no_telp_anggota' => $no_telp_anggota,
		'ket_anggota' => $ket_anggota,
		'rekening' => $rekening
		);
		$res = $this->mymodel->InsertData('tb_anggota',$data_insert);
		if($res>=1){
			$this->session->set_flashdata('pesan','Tambah Data Sukses ^_^ ');
			redirect('crud/index');
		} else{
			echo "<h2>Insert data Gagal</h2>";
		}
		
	}

	public function edit_data($id_anggota){
		$agt = $this->mymodel->GetAnggota("where id_anggota = '$id_anggota'");
		$data = array(
			"id_anggota" => $agt[0]['id_anggota'],
			"nama_anggota" => $agt[0]['nama_anggota'],
			"alamat_anggota" => $agt[0]['alamat_anggota'],
			"tempat_lahir_anggota" => $agt[0]['tempat_lahir_anggota'],
			"tgl_lahir_anggota" => $agt[0]['tgl_lahir_anggota'],
			"jenis_kelamin_anggota" => $agt[0]['jenis_kelamin_anggota'],
			"status_anggota" => $agt[0]['status_anggota'],
			"no_telp_anggota" => $agt[0]['no_telp_anggota'],
			"ket_anggota" => $agt[0]['ket_anggota'],
			"rekening" => $agt[0]['rekening'],
		);
		$this->load->view('form_edit', $data);

		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";

	}

	public function do_update(){
		$id_anggota = $_POST['id_anggota'];
		$nama_anggota = $_POST['nama_anggota'];
		$alamat_anggota = $_POST['alamat_anggota'];
		$tempat_lahir_anggota = $_POST['tempat_lahir_anggota'];
		$tgl_lahir_anggota = $_POST['tgl_lahir_anggota'];
		$jenis_kelamin_anggota = $_POST['jenis_kelamin_anggota'];
		$status_anggota = $_POST['status_anggota'];
		$no_telp_anggota = $_POST['no_telp_anggota'];
		$ket_anggota = $_POST['ket_anggota'];
		$rekening = $_POST['rekening'];
		$data_update = array(
		'nama_anggota' => $nama_anggota,
		'alamat_anggota' => $alamat_anggota,
		'tempat_lahir_anggota' => $tempat_lahir_anggota,
		'tgl_lahir_anggota' => $tgl_lahir_anggota,
		'jenis_kelamin_anggota' => $jenis_kelamin_anggota,
		'status_anggota' => $status_anggota,
		'no_telp_anggota' => $no_telp_anggota,
		'ket_anggota' => $ket_anggota,
		'rekening' => $rekening
		);
		$where = array('id_anggota' => $id_anggota);
		$res = $this->mymodel->UpdateData('tb_anggota',$data_update, $where);
		if($res>=1){
		$this->session->set_flashdata('pesan','Insert Data Sukses ^_^ ');

			redirect('crud/index');
		}
	}

	public function do_delete($id_anggota){
		$where = array('id_anggota' => $id_anggota);
		$res = $this->mymodel->DeleteData('tb_anggota', $where);
		if($res>=1){
		$this->session->set_flashdata('pesan','Hapus Data Sukses ^_^ ');

			redirect('crud/index');
		}
	}
}
