<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserCon extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('usermodel');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
	
	function login() {
		if($this->session->userdata('logged_in')) {
			redirect('homecon/home', 'refresh');
		} else {
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['status'] = $session_data['status'];
			$this->load->view('/site/login');
		}
	}
	
	function login1() {
		if($this->session->userdata('logged_in')) {
			redirect('homecon/home', 'refresh');
		} else {
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['status'] = $session_data['status'];
			$this->load->view('/site/login1');
		}
	}
	
	function veriflogin()
	{
		if($this->session->userdata('logged_in')) {
			redirect('homecon', 'refresh');
		} else {
			$this->form_validation->set_rules('username','Username','strip_tags|required|xss_clean');
			$this->form_validation->set_rules('password','Password','strip_tags|required|xss_clean|callback_checklogin');
			
			if($this->form_validation->run() == FALSE) {
				$session_data = $this->session->userdata('logged_in');
				$data['username'] = $session_data['username'];
				$data['status'] = $session_data['status'];
				$this->load->view('/site/login1');
			} else {
				redirect('homecon', 'refresh');
			}
		}
	}
	
	function checklogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$result = $this->usermodel->login($username, $password);
			
		if($result) {
			$session_array = array();
			foreach($result as $row) {
				$session_array = array(
				'username'=>$row->username,
				'status'=>$row->status
				);
				$this->session->set_userdata('logged_in', $session_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('checklogin', 'Invalid username or password');
			return false;
		}
	}
	
	function register() {
		if($this->session->userdata('logged_in')) {
			redirect('homecon/index', 'refresh');
		} else {
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['status'] = $session_data['status'];
			$this->load->view('/site/register');
		}
	}
	
	function register1() {
		if($this->session->userdata('logged_in')) {
			redirect('homecon/index', 'refresh');
		} else {
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$data['status'] = $session_data['status'];
			$this->load->view('/site/register1');
		}
	}
	
	function do_register() {
		echo $this->input->post('register');
		$this->form_validation->set_rules('username','Username','strip_tags|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password','Password','strip_tags|required|xss_clean');
		$this->form_validation->set_rules('repassword','Password Confirmation','strip_tags|required|xss_clean|callback_checkpassword');
		
		if($this->form_validation->run() == FALSE) {
			$this->load->view('site/register1');
		} else {
			if($this->usermodel->add_user()) {
				redirect('usercon/login1', 'refresh');
			} else {
				redirect('usercon/register1', 'refresh');
			}
		}
	}
	
	function checkpassword() {
		$password = $this->input->post('password');
		$repassword = $this->input->post('repassword');
		
		if($password == $repassword)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkpassword', 'Password tidak sama');
			return FALSE;
		}
	}
	
	function logout() {
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('usercon/login1', 'refresh');
	}
}

?>