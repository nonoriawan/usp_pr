<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index(){
		$data = $this->mymodel->GetAnggota();	
		$this->load->view('tabel',array('data' => $data));
	}
	public function insert(){
		$res = $this->mymodel->InsertData('tb_anggota',array(
			"id_anggota" => "30300082",
			"nama_anggota" => "NONO RIAWAN",
			"alamat_anggota" => "NGORO MOJOKERTO",
		));
		if($res >= 1){
			echo "<h2> Insert data SUKSES </h2>";
		}else{
			echo "<h2> Insert data GAGAL </h2>";
		}
	}

	public function update(){
		$res = $this->mymodel->UpdateData('tb_anggota',array(
			"tempat_lahir_anggota" => "TULUNGAGUNG",
			"tgl_lahir_anggota" => "1986-09-12",
		),array('id_anggota' => "30300082"));

		if($res >= 1){
			echo "<h2> Update data SUKSES </h2>";
		}else{
			echo "<h2> Update data GAGAL </h2>";
		}	
	}
	
	public function delete(){
		$res = $this->mymodel->DeleteData('tb_anggota',array('id_anggota' => "30300082"));

		if($res >= 1){
			echo "<h2> Delete data SUKSES </h2>";
		}else{
			echo "<h2> Delete data GAGAL </h2>";
		}	
	}

	public function panggil(){
		$data = $this->db->query ("select * from tb_anggota");
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/
		echo "Jumlah data = " .$data -> num_rows()."<hr/>";
		$row = $data->row();
		echo "NIK = ". $row->id_anggota."<br/>";
		echo "Nama = ". $row->nama_anggota."<br/>";
		echo "NIK = ". $row->alamat_anggota."<hr/>";

		/*foreach ($data->result_array() as $row){
			echo "NIK : ".$row['id_anggota']."<br/>";
			echo "Nama : ".$row['nama_anggota']."<br/>";
			echo "Alamat : ".$row['alamat_anggota']."<br/>";
			echo "<hr/>";
		}*/
	}
}
